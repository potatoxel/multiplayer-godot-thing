extends RigidBody

var last_ground_position: Vector3 = Vector3(0, 0, 0)

func _physics_process(delta):
	if not Networking.my_id == 1: return
	var collision = get_world().direct_space_state.intersect_ray(global_transform.origin, global_transform.origin + Vector3.DOWN, [self])
	if collision and linear_velocity.length_squared() <= 0.5:
		last_ground_position = transform.origin
		
	if transform.origin.y <= -40:
		$NetworkTransform.set_position(last_ground_position + Vector3.UP * 0.1, true)
		$NetworkTransform.set_linear_velocity(Vector3.ZERO)
