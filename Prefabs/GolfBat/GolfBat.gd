extends KinematicBody

var _player: RigidBody

var _initial_stick_distance: float = 3
var _stick_distance: float = _initial_stick_distance
var _shooting: bool = false
var _shooting_speed: float = 2
var _ball_radius: float = 1.5
var _shooting_impulse: float = 0
var _max_shooting_impulse: float = 30
var _stick_rotation: float = 0
var _time:float = 0

var _can_shoot: bool = false

func setup(player: RigidBody):
	_player = player

func _physics_process(delta: float):
	if not _player:
		return
	if not _can_shoot and not _shooting and _player.linear_velocity.length_squared() <= 1:
		_player.linear_velocity = Vector3.ZERO
		_can_shoot = true
		$CanvasLayer/GolfPower.show()
		_stick_distance = _initial_stick_distance
		_time = 0
		_shooting_impulse = 0
		$CanvasLayer/GolfPower/Slider.value = 0
	if _can_shoot and Input.is_action_pressed("space"):
		_shooting_impulse = abs(sin(_time))*_max_shooting_impulse
		$CanvasLayer/GolfPower/Slider.value = 100*(_shooting_impulse/_max_shooting_impulse)
		_time += delta
	if _can_shoot and Input.is_action_just_released("space"):
		_can_shoot = false
		_shooting = true
		$CanvasLayer/GolfPower.hide()
		_player.get_node("NetworkEntity").increment_data("strokes", 1)
	if _shooting:
		_stick_distance -= delta * _shooting_speed
		if _stick_distance <= _ball_radius:
			_stick_distance = 100
			_player.get_node("NetworkTransform").apply_impulse(Vector3.ZERO, flatten(_player.transform.origin - transform.origin).normalized() * _shooting_impulse)
			_shooting = false
	if Input.is_action_pressed("right"):
		_stick_rotation += delta
	if Input.is_action_pressed("left"):
		_stick_rotation -= delta
	
	transform.origin = _player.transform.origin + Vector3(0, 0, _stick_distance).rotated(Vector3.UP, _stick_rotation)
	rotation.y = _stick_rotation
	
func flatten(v: Vector3):
	return Vector3(v.x, 0, v.z)
