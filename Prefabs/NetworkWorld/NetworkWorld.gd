extends Node

signal world_loaded

func _ready():
	set_network_master(1)
	yield(get_tree(),"idle_frame")
	
	if is_network_master():
		emit_signal("world_loaded")
	else:
		rpc_id(1, "_remote_request_world")

func add_entity(entity: NetworkEntity):
	rpc("_remote_add_entity", entity.serialize())

func remove_entity(entity: NetworkEntity):
	rpc("_remote_remove_entity", entity.serialize().id)

func send_world_to(peer_id: int):
	print_debug("send_world_to>" + str(peer_id))
	for entity in get_tree().get_nodes_in_group("network_entities"):
		send_entity_to(entity, peer_id)
	rpc_id(peer_id, "_remote_on_world_loaded")

func send_entity_to(entity, peer_id: int):
	print_debug("send>" + str(entity.serialize()))
	rpc_id(peer_id, "_remote_add_entity", entity.serialize())

remote func _remote_add_entity(data):
	#Security Pls Fix
	print_debug("recieve>" + str(data))
	if has_node(data.id):
		print("duplicate thus not added")
		get_node(data.id).get_node("NetworkEntity").setup(data)
		return
	var prefab = load(data.prefab)
	var instance = prefab.instance()
	get_node(data.id.substr(0, data.id.find_last("/"))).add_child(instance)
	instance.get_node("NetworkEntity").setup(data)

remotesync func _remote_remove_entity(id):
	if has_node(id):
		get_node(id).queue_free()

remote func _remote_on_world_loaded():
	emit_signal("world_loaded")

remote func _remote_request_world():
	if is_network_master():
		send_world_to(get_tree().get_rpc_sender_id())
