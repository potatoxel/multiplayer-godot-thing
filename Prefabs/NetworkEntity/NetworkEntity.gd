class_name NetworkEntity
extends Node

signal on_setup(data)
signal on_set(key, value)

var _data = {}
var manual_commit = false
var _uncommited_data = {}
var _setup = false

func _enter_tree():
	_data.prefab = get_parent().filename
	_data.id = str(get_parent().get_path())
	_data.network_master = get_parent().get_network_master()
	add_to_group("network_entities")

func setup(data):
	_data = data
	var node_path = NodePath(_data.id)
	get_parent().name = node_path.get_name(node_path.get_name_count()-1)
	get_parent().set_network_master(_data.network_master)
	#yield(get_tree(), "idle_frame")
	emit_signal("on_setup", data)
	for key in data:
		emit_signal("on_set", key, data[key])
	_setup = true

func connect_setup(obj, setup_func, set_func):
	connect("on_setup", obj, setup_func)
	connect("on_set", obj, set_func)
	if _setup:
		obj.call(setup_func, _data)
		for key in _data:
			obj.call(set_func, key, _data[key])

func set_data(key, value, broadcast=true):
	if broadcast:
		if manual_commit:
			_uncommited_data[key] = value
		else:
			rpc("_remote_set_data", key, value)
	else:
		_data[key] = value
		emit_signal("on_set", key, value)

func get_data(key, default=null):
	if key in _uncommited_data:
		return _uncommited_data[key]
	elif key in _data:
		return _data[key]
	else:
		return default

func increment_data(key, amount, broadcast=true):
	set_data(key, get_data(key, 0) + amount, broadcast)

func commit_data():
	for key in _uncommited_data:
		rpc("_remote_set_data", key, _uncommited_data[key])
	_uncommited_data.clear()

func serialize():
	return _data

remotesync func _remote_set_data(key, value):
	_data[key] = value
	if key == "network_master":
		get_parent().set_network_master(int(value))
	emit_signal("on_set", key, value)
