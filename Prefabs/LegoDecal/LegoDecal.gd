extends Spatial

const Lego = preload("res://GamePrefabs/Lego/Lego.gd")

export(Color) var color setget _set_color,_get_color
export(bool) var image setget _set_image,_get_image
export(bool) var image_region setget _set_image_region,_get_image_region
export(Vector3) var direction setget _set_direction,_get_direction

func _ready():
	$MeshInstance.material_override = $MeshInstance.material_override.duplicate()
	$MeshInstance.mesh = $MeshInstance.mesh.duplicate()
	

func _on_NetworkEntity_on_set(key, value):
	if key == "color":
		($MeshInstance.material_override as SpatialMaterial).albedo_color = value
	if key == "image":
		($MeshInstance.material_override as SpatialMaterial).albedo_texture = load(value)
	if key == "image_region":
		var mat = ($MeshInstance.material_override as SpatialMaterial)
		var size = Vec.vec2_vec1(Vec.pairwise_div2(value.size, mat.albedo_texture.get_size()), 0)
		var pos = Vec.vec2_vec1(Vec.pairwise_div2(value.position, mat.albedo_texture.get_size()), 0)
		mat.uv1_scale = size
		mat.uv1_offset = pos
	if key == "image_direction":
		var parent = get_parent() as Lego
		#NOT TESTED
		if abs(value.y) > 0.01:
			$MeshInstance.mesh.size = Vector2(parent.size.x, parent.size.z)
		elif abs(value.x) > 0.01:
			$MeshInstance.mesh.size = Vector2(parent.size.y, parent.size.z)
		elif abs(value.z) > 0.01:
			$MeshInstance.mesh.size = Vector2(parent.size.x, parent.size.y)
		$MeshInstance.transform.origin = value * parent.size.y*0.55
		$MeshInstance.transform.basis.y = value
func set_color_locally(value):
	($MeshInstance.material_override as SpatialMaterial).albedo_color = value

###########################
func _set_color(value):
	$NetworkEntity.set_data("color", value)
func _get_color():
	return $NetworkEntity.get_data("color")
	
func _set_image(value):
	$NetworkEntity.set_data("image", value)
func _get_image():
	return $NetworkEntity.get_data("image")

func _set_image_region(value):
	$NetworkEntity.set_data("image_region", value)
func _get_image_region():
	return $NetworkEntity.get_data("image_region")

func _set_direction(value):
	$NetworkEntity.set_data("image_direction", value)
func _get_direction():
	return $NetworkEntity.get_data("image_direction")
