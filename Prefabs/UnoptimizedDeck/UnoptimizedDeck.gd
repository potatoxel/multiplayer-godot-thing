extends Spatial

var Lego = preload("res://GamePrefabs/Lego/Lego.tscn")
var LegoDecal = preload("res://Prefabs/LegoDecal/LegoDecal.tscn")
export(Texture) var cardImage  #= preload("res://Assets/textures/UNO-Front.png")
export(Texture) var cardBackImage #= preload("res://Assets/textures/UNO-Back.png")
export(Vector2) var cardBackPosition = Vector2(0, 0)

var _texture_card_size = Vector2(4096/10.0, 4096/7.0)

func init_card_position_array(arr):
	printerr("TODO IMPLEMENT")

func _ready():
	yield(get_tree().create_timer(0.1),"timeout")
	if Networking.my_id == 1:
		randomize()
		var i =0
		var arr = []
		init_card_position_array(arr)
		arr.shuffle()
		for pos in arr:
			var x = pos[0]
			var y = pos[1]
			
			var card = Lego.instance()
			card.name = str(i) + "_" + name + "_card"
			
			var physics_mat = PhysicsMaterial.new()
			physics_mat.friction = 1
			physics_mat.rough = true
			card.physics_material_override = physics_mat
			get_parent().add_child(card)
			card.anchored = true
			card.size = Vector3(1, 0.01, 2)
			card.transform.origin = transform.origin + (i)*Vector3.UP*card.size.y
			card.image = cardImage.resource_path
			card.image_region = Rect2(Vec.pairwise_mul2(Vector2(x, y), _texture_card_size), _texture_card_size)
			card.get_node("NetworkEntity").set_data("type", "card")
			$"../NetworkWorld".add_entity(card.get_node("NetworkEntity"))
			
			var cardTopDecal = LegoDecal.instance()
			card.add_child(cardTopDecal)
			cardTopDecal.image = cardBackImage.resource_path
			cardTopDecal.image_region = Rect2(Vec.pairwise_mul2(cardBackPosition, _texture_card_size), _texture_card_size)
			cardTopDecal.direction = Vector3.UP
			$"../NetworkWorld".add_entity(cardTopDecal.get_node("NetworkEntity"))
			
			i += 1
	queue_free()
