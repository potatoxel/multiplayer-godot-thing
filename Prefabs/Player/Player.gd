extends KinematicBody

var _interactables = []

func _ready():
	Networking.connect("player_left", self, "_on_player_left")

func _process(delta):
	var forward = global_transform.basis.xform(Vector3.FORWARD)
	#var right = global_transform.basis.xform(Vector3.RIGHT)
	if is_network_master():
		var target_linear_velocity = Vector3.DOWN
		var target_angular_velocity = 0
		if Input.is_action_pressed("forward"):
			target_linear_velocity += delta*256*forward
		if Input.is_action_pressed("backward"):
			target_linear_velocity += -delta*256*forward
		
		if Input.is_action_pressed("left"):
			global_rotate(Vector3(0, 1, 0), delta*PI/4)
		if Input.is_action_pressed("right"):
			global_rotate(Vector3(0, 1, 0), -delta*PI/4)
		
		if Input.is_action_just_pressed("space"):
			for interactable in _interactables:
				interactable.interact()
		
		move_and_slide(target_linear_velocity)

func _on_player_left(id):
	if get_network_master() == id:
		queue_free()


func _on_Area_body_entered(body):
	if body.has_method("interact"):
		_interactables.push_front(body)

func _on_Area_body_exited(body):
	_interactables.erase(body)
