extends Node

var _update_period = 1.0/20.0
var _time_left_update = 0
var _network_entity

var _prev_sleeping: bool = false

var _remote_position_last: float
var _remote_position_delta: float
var _remote_position: Vector3
remotesync func _remote_position_set(value, time, force=false):
	if value == null: return
	_remote_position = value
	if force or is_network_master() or (not owner is RigidBody and not owner is KinematicBody):
		owner.global_transform.origin = value
	
	_remote_position_delta = time - _remote_position_last
	_remote_position_last = time
	

var _remote_basis_last: float
var _remote_basis_delta: float
var _remote_basis: Basis
remotesync func _remote_basis_set(value, time):
	if not value: return	
	_remote_basis = value
	owner.global_transform.basis = value
	_remote_basis_delta = time - _remote_basis_last
	_remote_basis_last = time

var _remote_linear_last: float
var _remote_linear_delta: float
var _remote_linear_velocity: Vector3
remotesync func _remote_linear_velocity_set(value, time):
	if not value: return
	_remote_linear_velocity = value
	owner.linear_velocity = value
	_remote_linear_delta = time - _remote_linear_last
	_remote_linear_last = time

var _remote_angular_last: float
var _remote_angular_delta: float
var _remote_angular_velocity: Vector3
remotesync func _remote_angular_velocity_set(value, time):
	if not value: return
	_remote_angular_velocity = value
	owner.angular_velocity = value
	_remote_angular_delta = time - _remote_angular_last
	_remote_angular_last = time

remotesync func _remote_scale_set(value):
	owner.scale = value

#TODO
remote var _remote_torque setget _remote_torque_set
func _remote_torque_set(value):
	pass

remotesync func _remote_add_force(position, value):
	if owner is RigidBody:
		owner.add_force(value, position)

remotesync func _remote_apply_impulse(position, value):
	if owner is RigidBody:
		owner.apply_impulse(position, value)

func add_force(position, value):
	rpc("_remote_add_force", position, value)

func apply_impulse(position, value):
	rpc("_remote_apply_impulse", position, value)


func set_rigidbody_mode(mode: int):
	_network_entity.set_data("rigidbody_mode", mode)

func set_linear_velocity(velocity):
	rpc("_remote_linear_velocity_set", velocity, OS.get_ticks_msec()/1000)

func set_position(position, force=true, reliable=true):
	if position:
		if reliable:
			rpc("_remote_position_set", position, OS.get_ticks_msec()/1000, force)
		else:
			rpc_unreliable("_remote_position_set", position, OS.get_ticks_msec()/1000, force)
		owner.transform.origin = position
		

func broadcast_rotation(force=true, reliable=true):
	if reliable:
		rpc("_remote_basis_set", owner.global_transform.basis, OS.get_ticks_msec()/1000.0)
	else:
		rpc_unreliable("_remote_basis_set", owner.global_transform.basis, OS.get_ticks_msec()/1000.0)
	

func set_scale(scale, force=true):
	rpc("_remote_scale_set", scale)
	owner.scale = scale

func _ready():
	_remote_position = owner.global_transform.origin
	_remote_basis = owner.global_transform.basis
	_network_entity = owner.get_node("NetworkEntity")	
	_network_entity.connect_setup(self, "_network_entity_on_setup", "_network_entity_on_set")
	if owner is RigidBody:
		_prev_sleeping = owner.sleeping
	set_process(is_network_master())
	set_physics_process(is_network_master())
	
func _network_entity_on_set(key, value):
	if key == "rigidbody_mode":
		owner.mode = value
	if key == "sleeping":
		owner.sleeping = value

func _network_entity_on_setup(data):
	if "position" in data:
		owner.global_transform.origin = data.position
		_remote_position = data.position
		if owner is RigidBody:
			_remote_linear_velocity = Vector3.ZERO
			_remote_angular_velocity = Vector3.ZERO
			owner.linear_velocity = Vector3.ZERO
			owner.angular_velocity = Vector3.ZERO
	if "basis" in data:
		owner.global_transform.basis = data.basis
	if "scale" in data:
		owner.scale = data.scale
	set_process(true)
	set_physics_process(true)

func _process(delta):
	if Networking.my_id == 1:
		_network_entity.set_data("position", owner.global_transform.origin, false)
		_network_entity.set_data("basis", owner.global_transform.basis, false)
		_network_entity.set_data("scale", owner.scale, false)

func _physics_process(delta):
	if is_network_master():
		if _time_left_update <= 0:
			if owner is RigidBody:
				if owner.sleeping:
					return
				rpc_unreliable("_remote_linear_velocity_set", owner.linear_velocity, OS.get_ticks_msec()/1000.0)
				rpc_unreliable("_remote_angular_velocity_set", owner.angular_velocity, OS.get_ticks_msec()/1000.0)
			rpc_unreliable("_remote_position_set", owner.global_transform.origin, OS.get_ticks_msec()/1000.0, false)
			rpc_unreliable("_remote_basis_set", owner.global_transform.basis, OS.get_ticks_msec()/1000.0)
			
			_time_left_update = _update_period
		else:
			_time_left_update -= delta
	else:
		if owner is RigidBody:
			var delta_v = _remote_position - owner.global_transform.origin
			var delta_len = delta_v.length_squared() 
			if delta_len > 32:
				owner.global_transform.origin = _remote_position
			elif delta_len > 0.1 and _remote_linear_delta > 0:
				owner.linear_velocity = delta_v / _remote_linear_delta
			#owner.linear_velocity = _remote_linear_velocity*(1+_remote_basis_delta*10) #+ delta_v.normalized()*_remote_basis_delta*(1+delta_v.length())*14
		elif owner is KinematicBody:
			var delta_v = _remote_position - owner.global_transform.origin
			if delta_v.length_squared() > 1:
				owner.global_transform.origin = _remote_position
			elif delta_v.length_squared() > 0.04:
				owner.move_and_slide(delta_v*6)
		
		#if (_remote_position - owner.global_transform.origin).length_squared() > 0.05:
		#if (_remote_basis.get_euler() - owner.global_transform.basis.get_euler()).length_squared() > 0.05:
