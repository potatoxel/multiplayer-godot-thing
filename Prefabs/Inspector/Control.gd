extends Node

signal on_changed
var value setget _set_value, _get_value
var label setget _set_label, _get_label

func _set_value(val):
	value = val

func _get_value():
	return value

func _set_label(val):
	if has_node("Label"):
		get_node("Label").text = val

func _get_label():
	if has_node("Label"):
		return get_node("Label").text
