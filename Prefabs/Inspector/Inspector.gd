extends Control

var _controls = {
	TYPE_STRING: preload("res://Prefabs/Inspector/Controls/Text/Text.tscn"),
	TYPE_COLOR: preload("res://Prefabs/Inspector/Controls/Color/Color.tscn"),
	TYPE_VECTOR3: preload("res://Prefabs/Inspector/Controls/Vector3/Vector3.tscn"),
	TYPE_BOOL: preload("res://Prefabs/Inspector/Controls/Boolean/Boolean.tscn")
}

func add_property(object, property_path, type = null):
	var property_item = type if type != null else typeof(object.get(property_path))
	if not _controls.has(property_item):
		return false
	var item = _controls[property_item].instance()
	item.label = property_path
	item.value = object.get(property_path)
	$Panel/VBoxContainer.add_child(item)
	item.connect("on_changed", self, "_on_property_changed", [object, property_path])
	return true

func _on_property_changed(value, object, property_path):
	object.set(property_path, value)
	if property_path == "translation":
		var network_transform = object.get_node_or_null("NetworkTransform")
		if network_transform:
			network_transform.set_position(value)

func add_all_exported_properties(object: Node):
	var properties = object.get_property_list()
	for property in properties:
		if property.usage == 8199:
			add_property(object, property.name, property.type)

func clear():
	for child in $Panel/VBoxContainer.get_children():
		child.queue_free()

func inspect(object: Node):
	clear()
	add_all_exported_properties(object)
	if object is Spatial:
		add_property(object, "translation", TYPE_VECTOR3)

