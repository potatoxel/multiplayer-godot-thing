extends "res://Prefabs/Inspector/Control.gd"



func _on_TextEdit_text_changed():
	emit_signal("on_changed", $TextEdit.text)

func _set_value(value):
	$TextEdit.text = str(value)

func _get_value():
	return $TextEdit.text
