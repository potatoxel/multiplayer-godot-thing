extends "res://Prefabs/Inspector/Control.gd"


func _on_CheckBox_toggled(button_pressed):
	emit_signal("on_changed", button_pressed)

func _set_value(value):
	$CheckBox.pressed = value

func _get_value():
	return $CheckBox.pressed

