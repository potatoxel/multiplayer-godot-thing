extends "res://Prefabs/Inspector/Control.gd"



func _set_value(value):
	$VBoxContainer/HBoxContainer/x.text = str(value.x) if value else "0"
	$VBoxContainer/HBoxContainer2/y.text = str(value.y) if value else "0"
	$VBoxContainer/HBoxContainer3/z.text = str(value.z) if value else "0"

func _get_value():
	return Vector3(
		float($VBoxContainer/HBoxContainer/x.text),
		float($VBoxContainer/HBoxContainer2/y.text),
		float($VBoxContainer/HBoxContainer3/z.text)
	)


func _on_x_text_changed():
	emit_signal("on_changed", _get_value())

func _on_y_text_changed():
	emit_signal("on_changed", _get_value())

func _on_z_text_changed():
	emit_signal("on_changed", _get_value())
