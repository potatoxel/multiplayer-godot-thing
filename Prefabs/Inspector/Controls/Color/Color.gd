extends "res://Prefabs/Inspector/Control.gd"


func _on_ColorPickerButton_color_changed(color):
	emit_signal("on_changed", color)

func _set_value(value):
	if value:
		$ColorPickerButton.color = value

func _get_value():
	return $ColorPickerButton.color
