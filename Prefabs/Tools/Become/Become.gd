extends Node

var selected_object

func is_tool():
	return true

func get_raycast_ignores():
	return []

func is_dragging():
	return false

func mouse_left_down(object: Spatial, position: Vector3):
	selected_object = object
	var scene = object.get_tree().current_scene
	var simple_look = scene.get_node("SimpleLookCamera")
	var follow_camera = scene.get_node("FollowCamera")
	simple_look.current = not simple_look.current
	follow_camera.current = not simple_look.current
	follow_camera.target = object

func process(delta):
	pass

func mouse_drag(object: Spatial, position: Vector3):
	pass

func mouse_left_release():
	pass
