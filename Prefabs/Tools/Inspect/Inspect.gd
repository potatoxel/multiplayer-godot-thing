extends Node

var selected_object

func is_tool():
	return true

func get_raycast_ignores():
	return []

func is_dragging():
	return false

func mouse_left_down(object: Spatial, position: Vector3):
	selected_object = object
	object.get_tree().current_scene.get_node("CanvasLayer/Inspector").inspect(object)

func process(delta):
	pass

func mouse_drag(object: Spatial, position: Vector3):
	pass

func mouse_left_release():
	pass
