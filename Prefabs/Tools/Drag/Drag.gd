extends Node

var _raycast_ignores = []
var _dragging: Spatial
var _obj_y: float = 0
var _obj_floor_offset_y
var _rigidbody_mode 
var _mouse_position_3d

func is_tool():
	return true

func get_raycast_ignores():
	return _raycast_ignores

func is_dragging():
	return _dragging != null

func mouse_left_down(object: Spatial, position: Vector3):
	_raycast_ignores = [object]
	_dragging = object
	_obj_y = object.transform.origin.y
	_mouse_position_3d = null
	var network_transform = _dragging.get_node("NetworkTransform")
	if _dragging is RigidBody:
		_rigidbody_mode = _dragging.mode
		network_transform.set_rigidbody_mode(_dragging.MODE_STATIC)

func process(delta):
	if is_dragging():
		var network_transform = _dragging.get_node("NetworkTransform")
		if not network_transform: return
		
		if Input.is_action_just_pressed("instant_flip"):
			_dragging.rotate_object_local(Vector3.RIGHT, PI)
			network_transform.broadcast_rotation()
		if Input.is_action_pressed("flip"):
			_dragging.rotate_object_local(Vector3.RIGHT, PI/32)
			network_transform.broadcast_rotation()
		if Input.is_action_pressed("rotate_right"):
			_dragging.rotate(Vector3.UP, PI/32)
			network_transform.broadcast_rotation()
		if Input.is_action_pressed("rotate_left"):
			_dragging.rotate(Vector3.UP, -PI/32)
			network_transform.broadcast_rotation()
		if Input.is_action_just_pressed("reset_rotation"):
			_dragging.rotate_x(-_dragging.rotation.x)
			_dragging.rotate_y(-_dragging.rotation.y)
			_dragging.rotate_z(-_dragging.rotation.z)
			network_transform.broadcast_rotation()
		if _mouse_position_3d:
			_update_position(network_transform, _mouse_position_3d)

func mouse_drag(object: Spatial, position: Vector3):
	if not _obj_floor_offset_y:
		_obj_floor_offset_y = _obj_y - position.y
	var network_transform = _dragging.get_node("NetworkTransform")
	if network_transform:
		_mouse_position_3d = position
		_update_position(network_transform, _mouse_position_3d)

func _update_position(network_transform, position):
		var offset
		if _dragging.has_method("get_aabb"):
			offset = Vector3.UP * _dragging.get_aabb().size.y/2
		else:
			offset = Vector3.UP*(_obj_floor_offset_y)
		network_transform.set_position(position + offset)
		
func mouse_left_release():
	if _dragging is RigidBody:
		var network_transform = _dragging.get_node("NetworkTransform")
		network_transform.set_rigidbody_mode(_rigidbody_mode)
	
	_raycast_ignores = []
	_dragging = null
	_obj_y = 0
	_obj_floor_offset_y = null
		
