extends Node

var _plane: Plane
var _camera: Camera

func _ready():
	_plane = Plane(Vector3.UP, 0.2)

func _input(event):
	if is_network_master():
		if event is InputEventMouse:
			_camera = get_viewport().get_camera()
			var from = _camera.project_ray_origin(event.position)
			var to = from + _camera.project_ray_normal(event.position) * 3000
			var point = _plane.intersects_ray(from, to)
			$"../NetworkTransform".set_position(point, true, false)
