tool
extends EditorPlugin


func _enter_tree():
	var files = list_files_in_directory("res://Assets/golf/")
	for file in files:
		if not file.ends_with(".glb"): continue
		print(file)
		var scene = load("res://Assets/golf/" + file).instance() as Spatial
		var mesh_instance = scene.get_child(0).get_child(0) as MeshInstance
		var mesh = mesh_instance.mesh
		var packet_scene = PackedScene.new()
		var shape = mesh.create_trimesh_shape()
		var collision = CollisionShape.new()
		collision.shape = shape
		var static_body = StaticBody.new()
		static_body.add_child(collision)
		mesh_instance.add_child(static_body)
		collision.owner = mesh_instance
		static_body.owner = mesh_instance
		packet_scene.pack(mesh_instance)
		ResourceSaver.save("res://Assets/golf_fixed/" + file.substr(0, len(file)-4) + ".scn", packet_scene)

func _exit_tree():
	pass

func list_files_in_directory(path):
	var files = []
	var dir = Directory.new()
	dir.open(path)
	dir.list_dir_begin()

	while true:
		var file = dir.get_next()
		if file == "":
			break
		elif not file.begins_with("."):
			files.append(file)

	dir.list_dir_end()

	return files
