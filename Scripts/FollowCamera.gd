extends Camera

var target: Spatial
var offset = Vector3(0, 6, 10)

func _process(delta):
	if current and target:
		transform.origin = target.transform.origin + offset.rotated(Vector3.UP, target.rotation.y)
		look_at(target.transform.origin, Vector3.UP)
