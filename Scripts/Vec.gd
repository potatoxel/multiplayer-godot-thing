extends Node
class_name Vec

static func flatten(v: Vector3):
	return Vector3(v.x, 0, v.z)

static func pairwise_mul3(a: Vector3, b: Vector3):
	return Vector3(a.x * b.x, a.y * b.y, a.z * b.z)
	
static func pairwise_mul2(a: Vector2, b: Vector2):
	return Vector2(a.x * b.x, a.y * b.y)
	
static func pairwise_div3(a: Vector3, b: Vector3):
	return Vector3(a.x / b.x, a.y / b.y, a.z / b.z)
	
static func pairwise_div2(a: Vector2, b: Vector2):
	return Vector2(a.x / b.x, a.y / b.y)

static func vec2_vec1(a: Vector2, b: float):
	return Vector3(a.x, a.y, b)
