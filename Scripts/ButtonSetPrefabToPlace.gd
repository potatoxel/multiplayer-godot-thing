extends Button

export(PackedScene) var prefab: PackedScene

func _ready():
	connect("button_up", self, "_on_press")

func is_tool(prefab):
	return prefab and prefab.has_method("is_tool") and prefab.is_tool()

func _on_press():
	if is_tool(Game.current_prefab_to_place):
		Game.current_prefab_to_place.free()
	var prefab_instance = prefab.instance()
	if is_tool(prefab_instance):
		Game.current_prefab_to_place = prefab_instance
	else:
		prefab_instance.free()
		Game.current_prefab_to_place = prefab
