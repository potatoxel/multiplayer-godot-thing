class_name RayUtils

static func raycast_from_camera(world, camera, mouse_position, raycast_ignores=[], distance=300):
	var from = camera.project_ray_origin(mouse_position)
	var to = from + camera.project_ray_normal(mouse_position) * distance
	var coll = world.direct_space_state.intersect_ray(from, to, raycast_ignores)
	return coll
