extends Spatial

var target: Spatial

func _process(delta):
	if target:
		transform.origin = target.transform.origin 
	if Input.is_action_pressed("camera_right"):
		rotate_y(PI/64)
	elif Input.is_action_pressed("camera_left"):
		rotate_y(-PI/64)
