extends Control

func _ready():
	Networking.connect("connection_success", self, "_on_connection_success")
	for game in FileUtils.list_files_in_directory("res://Scenes/Games/"):
		$Panel/GameList.add_item(game)
	$Panel/GameList.select(0)

func _on_connection_success(success):
	if success:
		if Networking.my_id == 1:
			var game = $Panel/GameList.get_item_text($Panel/GameList.get_selected_items()[0])
			Networking.current_game = game

func _on_JoinServerBtn_pressed():
	Networking.join_server($Panel/IPAddress.text, $Panel/Name.text)

func _on_HostServerBtn_pressed():
	Networking.host_server($Panel/Name.text)

func _on_WebSocketsCheckBox_toggled(button_pressed):
	Networking.websockets = button_pressed
