extends Spatial

var count = 0 setget _set_count, _get_count

func is_in_bounds(value):
	return 0 <= value and value <= 5

func _set_count(value):
	if 0 <= count and count <= 5:
		var old = count
		count = value
		$AnimationPlayer.play(str(value),0,0,true)

func increment_count(speed):
	if is_in_bounds(count+1):
		count += 1
		$AnimationPlayer.play(str(count), -1, speed)
		return true
	return false

func _get_count():
	return count
