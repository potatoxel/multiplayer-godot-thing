extends Spatial

const FiveTally = preload("res://Scenes/Games/Letterfill/Prefabs/TallyCounter/FiveTally.tscn")
var current
var columns = 20
var count = 0

func _ready():
	current = $Tally/FiveTally0
	
func increment_count(speed = 4, broadcast = true):
	if broadcast:
		$NetworkEntity.set_data("count", count+1)
	else:
		count += 1
		if not current.increment_count(speed):
			var position = Vector3(
				(count-1)/5 % columns,
				0,
				floor((count-1)/5 / columns)
			)
			current = FiveTally.instance()
			current.transform.origin = position * 0.9 * 2
			$Tally.add_child(current)
			current.increment_count(4)
		yield(current.get_node("AnimationPlayer") as AnimationPlayer, "animation_finished")

func increment_count_by(amount: float, total_speed = 4, broadcast = true):
	if broadcast:
		$NetworkEntity.set_data("count", count+amount)
	else:
		for i in amount:
			yield(increment_count(total_speed*amount, false), "completed")

func set_count(value, broadcast=true):
	for child in $Tally.get_children():
		child.queue_free()
	count = 0
	for i in floor(value/5.0)+1:
		var position = Vector3(
			(count)/5 % columns,
			0,
			floor((count)/5 / columns)
		)
		current = FiveTally.instance()
		current.transform.origin = position * 0.9 * 2
		$Tally.add_child(current)
		current.count = 5
		count += 5
	count = value
	current.count = value % 5
	if broadcast:
		$NetworkEntity.set_data("count", value)

func _on_NetworkEntity_on_setup(data):
	set_count(int(data.count), false)

func _on_NetworkEntity_on_set(key, value):
	if key == "count":
		if value < count:
			set_count(int(value), false)
		elif value > count:
			increment_count_by(int(value)-count, 4, false)
