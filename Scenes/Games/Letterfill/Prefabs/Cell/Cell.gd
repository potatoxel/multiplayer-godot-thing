extends CSGBox

const Piece = preload("res://Scenes/Games/Letterfill/Prefabs/Piece/Piece.tscn")


var piece_count = 0 setget _set_piece_count, _get_piece_count
var _pieces_grid = Vector2(2,2)
var piece_color = Color.blue setget _set_piece_color, _get_piece_color

func _ready():
	render_pieces()


func _on_NetworkEntity_on_set(key, value):
	if key == "piece_count" or key =="piece_color":
		render_pieces()


func render_pieces():
	for child in get_children():
		child.queue_free()
	for i in self.piece_count:
		var x = i % int(_pieces_grid.x)
		var y = floor(i / _pieces_grid.y)
		var piece = Piece.instance()
		add_child(piece)
		
		var material = piece.material as SpatialMaterial
		material = material.duplicate()
		material.albedo_color = self.piece_color
		piece.material = material
		
		var grid_offset = Vector2(
			(width - (piece.width*1.05) * _pieces_grid.x - 0.05*piece.width)/2.0,
			(depth - (piece.depth*1.05) * _pieces_grid.y - 0.05*piece.depth)/2.0
		)
		
		piece.transform.origin = Vector3(
			x * (piece.width*1.05) - grid_offset.x,
			0,
			y * (piece.depth*1.05) - grid_offset.y
		)


func _set_piece_count(value):
	if 0 <= value and value <= _pieces_grid.x * _pieces_grid.y:
		$"../NetworkEntity".set_data("piece_count", value)
		render_pieces()

func _get_piece_count():
	return $"../NetworkEntity".get_data("piece_count", 0)

func _set_piece_color(value):
	$"../NetworkEntity".set_data("piece_color", value)
	render_pieces()

func _get_piece_color():
	return $"../NetworkEntity".get_data("piece_color", Color.blue) 
