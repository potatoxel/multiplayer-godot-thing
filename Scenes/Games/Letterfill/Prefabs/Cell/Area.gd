extends StaticBody

signal pressed

onready var width = $Cell.width
onready var depth = $Cell.depth

func _input_event(camera, event, position, normal, shape_idx):
	if event is InputEventMouseButton:
		if event.pressed:
			add_piece()
			emit_signal("pressed")

func add_piece():
	$Cell.piece_count += 1
