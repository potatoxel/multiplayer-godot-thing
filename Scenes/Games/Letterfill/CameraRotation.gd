extends Spatial

func _unhandled_key_input(event):
	if event is InputEventKey:
		if event.is_action_pressed("right"):
			rotate_y(PI/4)
		elif event.is_action_pressed("left"):
			rotate_y(-PI/4)
