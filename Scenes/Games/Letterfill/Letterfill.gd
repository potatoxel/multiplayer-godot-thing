extends Spatial

const Cell = preload("res://Scenes/Games/Letterfill/Prefabs/Cell/Cell.tscn")

var _grid_size = Vector2(5, 5)
var my_color = Color.red

func _ready():
	if Networking.my_id == 1:
		for x in _grid_size.x:
			for y in _grid_size.y:
				var cell = Cell.instance()
				cell.name = str(x) + " " + str(y) + "_cell"
				add_child(cell)
				$NetworkWorld.add_entity(cell.get_node("NetworkEntity"))
				cell.transform.origin = Vector3(
					x * (cell.width*1.05) - 0.5 * (_grid_size.x-1) * cell.width,
					0, 
					y * (cell.depth*1.05) - 0.5 * (_grid_size.y-1) * cell.depth
				)
				cell.connect("pressed", self, "_on_cell_pressed", [cell])
	else:
		$NetworkWorld.connect("world_loaded", self, "_on_world_loaded")

func _on_world_loaded():
	for child in get_children():
		if child.name.ends_with("_cell"):
			child.connect("pressed", self, "_on_cell_pressed", [child])

func _on_cell_pressed(cell):
	cell.get_node("Cell").piece_color = my_color

func _on_ColorList_item_selected(index):
	var color_name = $ColorList.get_item_text(index)
	if color_name == "Red":
		my_color = Color(1, 0 ,0)
	elif color_name == "Green":
		my_color = Color(0, 1, 0)
	elif color_name == "Blue":
		my_color = Color(0, 0, 1)
	elif color_name == "Purple":
		my_color = Color.purple
	elif color_name == "Cyan":
		my_color = Color.cyan

func _on_Button_pressed():
	$TallyCounter.increment_count()

func _on_Button_right_pressed():
	$TallyCounter.increment_count_by(5)

func _on_Button2_pressed():
	$TallyCounter2.increment_count()

func _on_Button2_right_pressed():
	$TallyCounter2.increment_count_by(5)

func _on_Button3_pressed():
	$TallyCounter3.increment_count()

func _on_Button3_right_pressed():
	$TallyCounter3.increment_count_by(5)

func _on_Button4_pressed():
	$TallyCounter4.increment_count()

func _on_Button4_right_pressed():
	$TallyCounter4.increment_count_by(5)
