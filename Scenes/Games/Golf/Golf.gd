extends Spatial

const Player = preload("res://Prefabs/GolfBall/GolfBall.tscn")

var player
onready var camera_pivot = $CameraPivot
var free_prefab_id = 0
onready var _grid_map = $GridMap as GridMap

var holes = []
var spawns = []
var _current_hole = 0
signal goal(player, hole_pos)

var _scores_table: GridContainer
var _scores: Dictionary = {}
var round_done = false

func _ready():
	$NetworkWorld.connect("world_loaded", self, "_on_world_load")
	for pos in _grid_map.get_used_cells():
		var item = _grid_map.get_cell_item(pos.x, pos.y, pos.z)
		var mesh_name: String = _grid_map.mesh_library.get_item_name(item)
		var world_pos = _grid_map.map_to_world(pos.x, pos.y, pos.z) \
							+ _grid_map.global_transform.origin
		
		if mesh_name.find("hole") >= 0:
			var area: Area = Area.new()
			var collision_shape: CollisionShape = CollisionShape.new()
			var cylinder_shape = CylinderShape.new()
			cylinder_shape.radius = 0.4
			cylinder_shape.height = 0.9
			collision_shape.shape = cylinder_shape
			area.add_child(collision_shape)
			add_child(area)
			area.global_transform.origin = world_pos
			area.connect("body_entered", self, "_on_area_body_entered", [pos])
			holes.append(world_pos)
		
		if mesh_name == "club_blue":
			_grid_map.set_cell_item(pos.x, pos.y, pos.z, -1)
			spawns.append(world_pos)
	holes.sort_custom(self, "_sort_x")
	spawns.sort_custom(self, "_sort_x")
	
	_scores_table = $CanvasLayer/Scores/Table
	_scores_table.columns = 1 + holes.size()
	

func draw_table():
	for child in _scores_table.get_children():
		child.queue_free()
	for id in Networking.players_info:
		var player_name = Label.new()
		player_name.text = Networking.players_info[id].name
		player_name.size_flags_horizontal = Label.SIZE_EXPAND		
		_scores_table.add_child(player_name)
		for hole in _scores[id]:
			var score = _scores[id][hole]
			var txt = Label.new()
			txt.text = str(score)
			txt.size_flags_horizontal = Label.SIZE_EXPAND
			_scores_table.add_child(txt)
		for i in _scores_table.columns - (1 + _scores[id].size()):
			var txt = Label.new()
			txt.text = "/"
			txt.size_flags_horizontal = Label.SIZE_EXPAND
			_scores_table.add_child(txt)
	

func _on_world_load():
	player = Player.instance()
	player.set_network_master(Networking.my_id)
	player.name = str(Networking.my_id) + "_player"
	add_child(player)
	player.global_transform.origin = spawns[0]
	player.last_ground_position = spawns[0]
	$NetworkWorld.add_entity(player.get_node("NetworkEntity"))
	camera_pivot.target = player
	
	$GolfBat.setup(player)
	
func _sort_x(a,b):
	return a.x < b.x

func _on_area_body_entered(body, hole_pos):
	if not Networking.my_id == 1: return
	if round_done: return
	if body.name.ends_with("player"):
		var network_transform = body.get_node("NetworkTransform")
		network_transform.set_rigidbody_mode(RigidBody.MODE_STATIC)
		network_transform.set_linear_velocity(Vector3.ZERO)
		network_transform.set_position(holes[_current_hole], true)
		emit_signal("goal", body, hole_pos)
		
		var network_entity = body.get_node("NetworkEntity")
		network_entity.set_data("round_done", true)
		
		round_done = true
		for player_id in Networking.players_info:
			var obj = get_node(str(player_id) + "_player")
			if not obj.get_node("NetworkEntity").get_data("round_done"):
				round_done = false
		if round_done:
			rpc("_remote_show_table", true, _current_hole)
			yield(get_tree().create_timer(5), "timeout")
			rpc("_remote_show_table", false, _current_hole)
			_current_hole += 1
			for player_id in Networking.players_info:
				var obj = get_node(str(player_id) + "_player")
				var entity = obj.get_node("NetworkEntity")
				entity.set_data("round_done", false)
				entity.set_data("strokes", 0)
				var network_transform2 = obj.get_node("NetworkTransform")
				network_transform2.set_rigidbody_mode(RigidBody.MODE_RIGID)
				network_transform2.set_linear_velocity(Vector3.ZERO)
				network_transform2.set_position(spawns[_current_hole], true)
				obj.last_ground_position = spawns[_current_hole]
			yield(get_tree().create_timer(1), "timeout")
			round_done = false

remotesync func _remote_show_table(show, current_hole):
	if show:
		for player_id in Networking.players_info:
			var obj = get_node(str(player_id) + "_player")
			var entity = obj.get_node("NetworkEntity")
			if not player_id in _scores:
				_scores[player_id] = {}
			_scores[player_id][current_hole] = entity.get_data("strokes")
		draw_table()
		$CanvasLayer/Scores.show()
	else:
		$CanvasLayer/Scores.hide()
