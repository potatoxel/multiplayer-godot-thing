extends Node

signal money_changed

var money: float setget _set_money, _get_money
var provinces: Array = [] #<int>

func _ready():
	self.money = 100

func _set_money(value):
	$NetworkEntity.set_data("money", value)
	emit_signal("money_changed")

func _get_money():
	return $NetworkEntity.get_data("money")


func take_money(amount):
	if self.money >= amount:
		self.money -= amount
		return true
	else:
		return false
