extends Node

signal province_name_changed
signal population_changed
signal trade_changed
signal manpower_changed

var province_name: String setget _set_province_name, _get_province_name
var population: int setget _set_population, _get_population
var trade: int setget _set_trade, _get_trade
var manpower: int setget _set_manpower, _get_manpower

func _ready():
	$NetworkEntity.manual_commit = true

func _set_province_name(value):
	$NetworkEntity.set_data("province_name", value)
	emit_signal("province_name_changed")
func _set_population(value):
	$NetworkEntity.set_data("population", value)
	emit_signal("population_changed")
func _set_trade(value):
	$NetworkEntity.set_data("trade", value)
	emit_signal("trade_changed")
func _set_manpower(value):
	$NetworkEntity.set_data("manpower", value)
	emit_signal("manpower_changed")

func _get_province_name():
	return $NetworkEntity.get_data("province_name")
func _get_population():
	return $NetworkEntity.get_data("population")
func _get_trade():
	return $NetworkEntity.get_data("trade")
func _get_manpower():
	return $NetworkEntity.get_data("manpower")

func commit_data():
	$NetworkEntity.commit_data()
