extends StaticBody

#FIX ISSUE WITH RGB OR RGBA
#EITHER CORRECT IT AUTOMATICALLY
#OR ERROR if not RGB/RGBA (Pick one)

export(NodePath) var province_menu_path
onready var province_menu: Panel = get_node(province_menu_path)

const MapTexture: Texture = preload("res://Scenes/Games/War/GermanyMapProvinces@0.3x.png")
const PlayerMapTexture: Texture = preload("res://Scenes/Games/War/GermanyMapPlayers@0.3x.png")
const Province = preload("res://Scenes/Games/War/Prefabs/Province/Province.tscn")

var _map_pixels: PoolByteArray
var _map_player_pixels: PoolByteArray
var _map_size: Vector2 
var _map_rendered_texture: Texture
var _map_json_data
var _map_color_to_data: Dictionary
var _map_player_color_to_data: Dictionary
var _map_player_id_to_data: Dictionary
var _current_player_id = 1

func get_province_node_name_from_color(color):
	return str(color).replace(".", "D") + "_province"

func is_allowed_to_control_province(player_id, position):
	var player_color = _get_map_pixel(_map_player_pixels, position)
	
	if not player_color in _map_player_color_to_data:
		return false
	
	var data = _map_player_color_to_data[player_color]
	
	while data and data.Liege != null and data.ID != player_id:
		data = _map_player_id_to_data[data.Liege]
	return data and data.ID == _current_player_id



func _ready():
	var file = File.new()
	file.open("res://Scenes/Games/War/GermanyMap.json", File.READ)
	_map_json_data = JSON.parse(file.get_as_text()).result
	file.close()
	
	for province in _map_json_data.provinces:
		var color = Color8(province["Color"][0], province["Color"][1], province["Color"][2])
		_map_color_to_data[color] = province
	
	for player in _map_json_data.players:
		var color = Color8(player["Color"][0], player["Color"][1], player["Color"][2])
		_map_player_color_to_data[color] = player
		_map_player_id_to_data[player.ID] = player
	
	
	var data = MapTexture.get_data()
	data.lock()
	_map_size = Vector2(data.get_width(), data.get_height())
	_map_pixels = data.get_data()
	print(_map_pixels.size())
	data.unlock()
	
	var players_map_data = PlayerMapTexture.get_data()
	players_map_data.lock()
	_map_player_pixels = players_map_data.get_data()
	players_map_data.unlock()

	var map_rendered_image = Image.new()
	map_rendered_image.create(_map_size.x, _map_size.y, false, Image.FORMAT_RGB8)
	map_rendered_image.lock()
	for x in _map_size.x:
		for y in _map_size.y:
			var pos = Vector2(x, y)
			var color = _get_map_pixel(_map_pixels, pos)
			var res = Color.pink
			if color == Color.white:
				res = Color.deepskyblue
			elif color == Color.black:
				res = Color.black
			else:
				res = Color.darkgreen
			map_rendered_image.set_pixelv(pos, res)
	map_rendered_image.unlock()
	
	_map_rendered_texture = ImageTexture.new()
	_map_rendered_texture.create_from_image(map_rendered_image)
	
	($Map.material as SpatialMaterial).albedo_texture = _map_rendered_texture
	
	if Networking.my_id == 1:
		yield(get_tree(), "idle_frame")
		for color in _map_color_to_data:
			var province = _map_color_to_data[color]
			var province_instance = Province.instance()
			province_instance.name = get_province_node_name_from_color(color)
			get_parent().add_child(province_instance)
			get_parent().get_node("NetworkWorld").add_entity(province_instance.get_node("NetworkEntity"))
			province_instance.province_name = province.Name
			province_instance.population = province.Pop
			province_instance.trade = province.Trade
			province_instance.manpower = province.Man
			province_instance.commit_data()

func _get_map_pixel(map_pixels, pos: Vector2):
	#Assume no alpha.
	var offset = (pos.x + _map_size.x*pos.y) * 4
	if 0 <= offset and offset < 4 * _map_size.x * _map_size.y:
		return Color8(map_pixels[offset], map_pixels[offset+1], map_pixels[offset+2])
	else:
		return Color.pink

func interact(world_position: Vector3):
	var local_position: Vector2 = vec3_to_vec2(world_position - global_transform.origin)
	var map_position: Vector2 = vec2_pfloor(
			vec2_pmul(
				vec2_pdiv(local_position, vec3_to_vec2($CollisionShape.shape.extents)),
				_map_size/2
			) + _map_size/2
		)
	map_position.x = _map_size.x - map_position.x
	map_position.y = _map_size.y - map_position.y
	var color = _get_map_pixel(_map_pixels, map_position)
	print(map_position)
	print(color)
	print(is_allowed_to_control_province(1, map_position))

	_map_rendered_texture = _generate_texture(color, Color.yellow)
	($Map.material as SpatialMaterial).albedo_texture = _map_rendered_texture
	
	var province = get_province_from_color(color)
	print(province)
	if province:
		province_menu.set_can_control(is_allowed_to_control_province(_current_player_id, map_position))
		province_menu.show_information(province)
	else:
		province_menu.hide()


func _generate_texture(color_to_highlight: Color, highlight_color: Color):
	var map_rendered_image = Image.new()
	map_rendered_image.create(_map_size.x, _map_size.y, false, Image.FORMAT_RGB8)
	map_rendered_image.lock()
	for x in _map_size.x:
		for y in _map_size.y:
			var pos = Vector2(x, y)
			var color = _get_map_pixel(_map_pixels, pos)
			var res = Color.pink
			if color == Color.white:
				res = Color.deepskyblue
			elif color == Color.black:
				res = Color.black
			elif color == color_to_highlight:
				res = highlight_color
			else:
				res = Color.darkgreen
			map_rendered_image.set_pixelv(pos, res)
	map_rendered_image.unlock()
	
	var map_rendered_texture = ImageTexture.new()
	map_rendered_texture.create_from_image(map_rendered_image)
	return map_rendered_texture

func get_province_from_color(color):
	var node_name = get_province_node_name_from_color(color)
	if get_owner().has_node(node_name):
		return get_owner().get_node(node_name)
	else:
		return null


#TODO PUT IN UTILS FILE
func vec3_to_vec2(v: Vector3):
	return Vector2(v.x, v.z)

func vec2_pmul(a: Vector2, b: Vector2):
	return Vector2(a.x*b.x, a.y*b.y)

func vec2_pdiv(a: Vector2, b: Vector2):
	return Vector2(a.x/b.x, a.y/b.y)

func vec2_pfloor(a: Vector2):
	return Vector2(floor(a.x), floor(a.y))
