extends Spatial

const Player = preload("res://Scenes/Games/War/Prefabs/Player/Player.tscn")
onready var camera = $CameraPivot/Camera

var player

func raycast_from_camera(camera, mouse_position, raycast_ignores=[], distance=300):
	var from = camera.project_ray_origin(mouse_position)
	var to = from + camera.project_ray_normal(mouse_position) * distance
	var coll = get_world().direct_space_state.intersect_ray(from, to, raycast_ignores)
	return coll

func _unhandled_input(event):
	if not (event is InputEventMouseButton): return
	var coll = raycast_from_camera(camera, event.position)
	
	if event.is_pressed():
		if coll.collider and coll.collider.has_method("interact"):
			print("Pressing on interactable at ", coll.position)
			coll.collider.interact(coll.position)

func _ready():
	$NetworkWorld.connect("world_loaded", self, "_on_world_load")

func _on_world_load():
	player = Player.instance()
	player.set_network_master(Networking.my_id)
	player.name = str(Networking.my_id) + "_player"
	player.connect("money_changed", self, "_on_player_money_changed", [player])
	
	add_child(player)
	
	$NetworkWorld.add_entity(player.get_node("NetworkEntity"))
	

func _on_player_money_changed(player):
	if player == self.player:
		$CanvasLayer/Stats/Money.text = "Money: " + str(player.money) + "f"


func _on_EndTurn_pressed():
	player.get_node("NetworkEntity").set_data("turn_done", true)
	$CanvasLayer/EndTurn.disabled = true
	var turn_done = true
	for child in get_children():
		if child.name.ends_with("_player"):
			if not child.get_node("NetworkEntity").get_data("turn_done"):
				turn_done = false
	if turn_done:
		rpc("_remote_end_turn")

remotesync func _remote_end_turn():
	for child in get_children():
		if child.name.ends_with("_province"):
			child.commit_data()
	player.get_node("NetworkEntity").set_data("turn_done", false)
	$CanvasLayer/EndTurn.disabled = false
