extends Panel

var current_province

func set_can_control(can_control):
	$ImproveButton.visible = can_control
	$ConscriptButton.visible = can_control

func show_information(province):
	current_province = province
	$Information.text = "Name: " + province.province_name + "\n" + \
						"Pop: " + str(province.population) + "\n" + \
						"Trade: " + str(province.trade) + "\n" + \
						"Man: " + str(province.manpower) + "\n"
	show()


func _on_ImproveButton_pressed():
	if get_owner().player.take_money(100):
		current_province.trade += 1
		show_information(current_province)


func _on_ConscriptButton_pressed():
	#TODO NETWORK	
	if current_province.population > 0 and get_owner().player.take_money(100):
		current_province.population -= 1
		current_province.manpower += 1
		show_information(current_province)
