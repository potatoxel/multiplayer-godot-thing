extends Spatial

var Player = preload("res://Prefabs/Hand/Hand.tscn")
var camera: Spatial
var free_prefab_id = 0

func _ready():
	var player = Player.instance()
	player.set_network_master(Networking.my_id)
	player.name = str(Networking.my_id) + "_player"
	add_child(player)
	$NetworkWorld.add_entity(player.get_node("NetworkEntity"))
	player.color = Color.red
	player.size = Vector3(0.5, 0.5, 0.5)
	Networking.my_player = player
	Networking.my_player_entity = player.get_node("NetworkEntity")
	
	#remove_child(camera)
	#player.add_child(camera)
	#camera.transform.origin = Vector3(0, 5, 5)


func _process(delta):
	if not Game.current_prefab_to_place: return
	var is_tool = Game.current_prefab_to_place.has_method("is_tool") \
		and Game.current_prefab_to_place.is_tool()
	if is_tool and Game.current_prefab_to_place.has_method("process"):
		Game.current_prefab_to_place.process(delta)

func _unhandled_input(event):
	if not (event is InputEventMouseButton or event is InputEventMouseMotion):
		return
	if not Game.current_prefab_to_place: return
	var is_tool = Game.current_prefab_to_place.has_method("is_tool") \
		and Game.current_prefab_to_place.is_tool()
	
	var raycast_ignores = []
	if is_tool:
		raycast_ignores = Game.current_prefab_to_place.get_raycast_ignores()
		
	camera = get_viewport().get_camera()
	
	var coll = RayUtils.raycast_from_camera(get_world(), camera, event.position, raycast_ignores)
	
	if event is InputEventMouseButton:
		if is_tool:
			Game.current_prefab_to_place.mouse_left_release()
	
	if not coll: return
	
	if event is InputEventMouseButton:
		if event.button_index == 1:
			if event.pressed:
				if is_tool:
					Game.current_prefab_to_place.mouse_left_down(coll.collider, coll.position)
				else:
					var obj = Game.current_prefab_to_place.instance()
					obj.name = str(Networking.my_id) + "_" + str(free_prefab_id)
					free_prefab_id += 1
					add_child(obj)
					$NetworkWorld.add_entity(obj.get_node("NetworkEntity"))
					if obj.has_node("NetworkTransform"):
						obj.get_node("NetworkTransform").set_position(coll.position)
	
	if event is InputEventMouseMotion:
		if is_tool and Game.current_prefab_to_place.is_dragging():
			Game.current_prefab_to_place.mouse_drag(coll.collider, coll.position)
