extends Panel

func is_tool(prefab):
	return prefab and prefab.has_method("is_tool") and prefab.is_tool()

func _ready():
	_add_directory("res://GamePrefabs/")

func _input(event):
	if event is InputEventKey:
		if event.is_action_pressed("ui_cancel"):
			visible = not visible

func _add_directory(dir: String):
	var added = 0
	for file in FileUtils.list_files_in_directory(dir):
		if file.find(".") == -1:
			var res = _add_directory(dir + file + "/")
			added += 1
			if File.new().file_exists(dir + file + "/" + file + ".tscn"):
				$VBoxContainer/ItemList.add_item(dir + file + "/" + file + ".tscn")
	return added

func _on_ItemList_item_activated(index):
	var text = $VBoxContainer/ItemList.get_item_text(index)
	
	var prefab = load(text)
	
	if prefab:
		if is_tool(Game.current_prefab_to_place):
			Game.current_prefab_to_place.free()
		var prefab_instance = prefab.instance()
		if is_tool(prefab_instance):
			Game.current_prefab_to_place = prefab_instance
		else:
			prefab_instance.free()
			Game.current_prefab_to_place = prefab
