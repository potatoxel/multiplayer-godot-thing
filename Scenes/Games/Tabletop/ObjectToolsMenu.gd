extends PopupMenu

var _object: Spatial
var _object_network_entity: NetworkEntity
var _object_network_transform
onready var _querying_thing = get_node("../../QueryingThing")

func _ready():
	add_item("Scale Up")
	add_item("Scale Down")
	add_separator()
	add_item("Shuffle")
	add_item("Flip")
	add_separator()
	add_item("Delete")
	
func _input(event):
	if event is InputEventMouseButton:
		if event.pressed and event.shift and event.button_index == BUTTON_RIGHT:
			var coll = RayUtils.raycast_from_camera(get_tree().current_scene.get_world(), get_viewport().get_camera(), event.position)
			if coll:
				_object = coll.collider
				if _object.has_node("NetworkEntity"):
					_object_network_entity = _object.get_node("NetworkEntity")
					_object_network_transform = _object.get_node("NetworkTransform")
				popup(Rect2(event.position, Vector2(100, 100)))
			else:
				_object = null
				_object_network_entity = null

func _on_ObjectToolsMenu_index_pressed(index):
	if not is_instance_valid(_object_network_entity): return
	
	
	match get_item_text(index):
		"Delete":
			get_tree().current_scene.get_node("NetworkWorld").remove_entity(_object_network_entity)
		"Scale Up":
			_object_network_transform.set_scale(_object.scale * 1.2)
		"Scale Down":
			_object_network_transform.set_scale(_object.scale / 1.2)
		"Flip":
			_object.rotate_object_local(Vector3.RIGHT, PI)
			_object_network_transform.broadcast_rotation()
		"Shuffle":
			var scene = get_tree().current_scene as Spatial
			var query = PhysicsShapeQueryParameters.new()
			
			_querying_thing.global_transform.origin = _object.global_transform.origin
			query.set_shape(_querying_thing.get_node("CollisionShape").shape)
			
			yield(get_tree(), "idle_frame")
			var results = scene.get_world().direct_space_state.intersect_shape(query, 200)
			_querying_thing.transform.origin = Vector3(10000, 10000, 10000)
			
			var min_y = INF
			for collision in results:
				if collision.collider is Lego and collision.collider.global_transform.origin.y < min_y:
					min_y = collision.collider.global_transform.origin.y
			
			results.shuffle()
			
			var y = min_y
			for collision in results:
				if collision.collider is Lego:
					collision.collider.global_transform.origin.y = y
					collision.collider.get_node("NetworkTransform").set_position(collision.collider.global_transform.origin)
					y += collision.collider.size.y
			
			
			
		
