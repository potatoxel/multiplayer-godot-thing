extends Spatial



func _on_Visibility_input_event(camera, event, position, normal, shape_idx):
	if event is InputEventMouseButton:
		if event.pressed and event.button_index == BUTTON_LEFT and not Networking.my_player_entity.get_data("got_land") and not $NetworkEntity.get_data("owner"):
			Networking.my_player_entity.set_data("got_land", true)
			$NetworkEntity.set_data("owner", Networking.my_id)

func _on_NetworkEntity_on_set(key, value):
	if key == "owner":
		$Viewport/CanvasLayer/Label.text = Networking.players_info[value].name
		$Visibility.mine = value == Networking.my_id


func _on_Visibility_body_entered(body):
	if body.has_node("NetworkEntity"):
		var entity = body.get_node("NetworkEntity")
		if entity.get_data("type") == "card":
			var net_transform =body.get_node("NetworkTransform")
			body.rotation = rotation
			body.rotate_object_local(Vector3.RIGHT, -2*PI/3)
			net_transform.broadcast_rotation()
