extends Camera

var speed = 10

func _process(delta):
	var right = Vec.flatten(transform.basis.xform(Vector3.RIGHT))
	var forward = Vec.flatten(transform.basis.xform(Vector3.UP))
	
	if Input.is_action_pressed("left"):
		transform.origin += -right * delta * speed
	if Input.is_action_pressed("right"):
		transform.origin += right * delta * speed
	if Input.is_action_pressed("forward"):
		transform.origin += forward * delta * speed
	if Input.is_action_pressed("backward"):
		transform.origin += -forward * delta * speed
	
