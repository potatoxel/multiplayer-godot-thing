extends Area

var readability = true
var mine = false

func _on_Visibility_body_entered(body):
	if body.has_node("NetworkEntity"):
		if not mine:
			if readability:
				if body.has_method("_set_color"):
					body.set_color_locally(Color.black)
			else:
				body.visible = false

func _on_Visibility_body_exited(body):
	if readability:
		if body.has_method("_set_color"):
			body.set_color_locally(Color.white)
	else:
		body.visible = true
