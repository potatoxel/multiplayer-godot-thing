extends Spatial

signal pressed
signal right_pressed
signal middle_pressed

func _on_StaticBody_input_event(camera, event, position, normal, shape_idx):
	if event is InputEventMouseButton:
		if event.pressed:
			rpc("_remote_press_button")
			if event.button_index == BUTTON_LEFT:
				emit_signal("pressed")
			elif event.button_index == BUTTON_RIGHT:
				emit_signal("right_pressed")
			elif event.button_index == BUTTON_MIDDLE:
				emit_signal("middle_pressed")

remotesync func _remote_press_button():
	$AnimationPlayer.play("Press", -1, 4)
