extends Node

onready var left_arm = $"../LeftArm"
onready var right_arm = $"../RightArm"
onready var left_leg = $"../LeftLeg"
onready var right_leg = $"../RightLeg"

onready var _previous_position: Vector3 = get_parent().transform.origin

var _distance_travelled = 0
var _max_distance_travelled = 2
var _speed_multiplier = 1

var _target_left_arm_rot_x = 0
var _target_right_arm_rot_x = 0
var _target_left_leg_rot_x = 0
var _target_right_leg_rot_x = 0
func _physics_process(delta):
	var velocity = (get_parent().transform.origin - _previous_position)/delta
	var speed = velocity.length()
	var signed_speed = velocity.dot(get_parent().transform.basis.z * -1)
	
	if speed > 0.01:
		_distance_travelled += signed_speed * delta * _speed_multiplier * 2
		_max_distance_travelled = 2
	elif abs(_distance_travelled) >= 0.5:
		_distance_travelled -= sign(_distance_travelled) * delta * 4
		_max_distance_travelled = 2
	else:
		_max_distance_travelled = 0.5
		_distance_travelled += _speed_multiplier * delta * 0.2
	
	if _distance_travelled >= _max_distance_travelled or _distance_travelled <= -_max_distance_travelled:
		_speed_multiplier *= -1
		_distance_travelled = _max_distance_travelled if _distance_travelled >= _max_distance_travelled else -_max_distance_travelled
	left_leg.rotation.x = atan2(2, _distance_travelled) - PI/2
	right_leg.rotation.x = -atan2(2, _distance_travelled) + PI/2
	
	if velocity.y < -0.1 || velocity.y > 0.1:
		_target_right_arm_rot_x = PI
		_target_left_arm_rot_x = PI
	else:
		_target_right_arm_rot_x = left_leg.rotation.x
		_target_left_arm_rot_x = right_leg.rotation.x
	
	if abs(right_arm.rotation.x - _target_right_arm_rot_x) > 0.1:
		right_arm.rotation.x += sign(_target_right_arm_rot_x-right_arm.rotation.x) * delta * PI*3
	if abs(left_arm.rotation.x - _target_left_arm_rot_x) > 0.1:
		left_arm.rotation.x += sign(_target_left_arm_rot_x-left_arm.rotation.x) * delta * PI*3
	
	_previous_position = get_parent().transform.origin
