extends RigidBody

export(bool) var local_player = false setget _set_local_player, _get_local_player

var _interactables = []
var _limbs = [
	$LeftArm,
	$RightArm,
	$LeftLeg,
	$RightLeg
]
func _ready():
	Networking.connect("player_left", self, "_on_player_left")

func _physics_process(delta):
	var forward = global_transform.basis.xform(Vector3.FORWARD)
	#var right = global_transform.basis.xform(Vector3.RIGHT)
	if self.local_player:
		var target_linear_velocity = Vector3.ZERO
		var target_angular_velocity = 0
		var target_torque = 0
		
		if Input.is_action_pressed("forward"):
			target_linear_velocity += delta*512*forward
		if Input.is_action_pressed("backward"):
			target_linear_velocity += -delta*512*forward
		
		if Input.is_action_pressed("left"):
			angular_velocity.y = PI
		elif Input.is_action_pressed("right"):
			angular_velocity.y = -PI
		else:
			angular_velocity.y = 0
		
		if Input.is_action_just_pressed("space"):
			apply_central_impulse(Vector3.UP*20)
		
		apply_central_impulse(
			_flatten(target_linear_velocity)-_flatten(linear_velocity)
		)

func _set_local_player(value):
	local_player = value
	if value:
		$NetworkEntity.set_data("network_master", Networking.my_id)

func _get_local_player():
	return $NetworkEntity.get_data("network_master") == Networking.my_id and local_player

func _on_player_left(id):
	if get_network_master() == id:
		queue_free()

func _flatten(v):
	return Vector3(v.x, 0, v.z)

#TODO: NETWORK
func break_joints():
	for limb in _limbs:
		limb.queue_free()
