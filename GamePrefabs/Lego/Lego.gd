extends RigidBody
class_name Lego

export(Vector3) var size = Vector3(1,1,1) setget _set_size,_get_size
export(Color) var color setget _set_color,_get_color
export(bool) var anchored setget _set_anchored,_get_anchored
export(String) var image setget _set_image,_get_image
export(Rect2) var image_region setget _set_image_region,_get_image_region


func _ready():
	$CollisionShape.shape = $CollisionShape.shape.duplicate()
	$Box.material_override = $Box.material_override.duplicate()
	$Box.mesh = $Box.mesh.duplicate()

func _on_NetworkEntity_on_set(key, value):
	if key == "size":
		var mesh = $Box.mesh
		mesh.size = value
		
		$CollisionShape.shape.extents = value/2
	if key == "color":
		($Box.material_override as SpatialMaterial).albedo_color = value
	if key == "anchored":
		mode = MODE_STATIC if value else MODE_RIGID
	if key == "image":
		($Box.material_override as SpatialMaterial).albedo_texture = load(value)
	if key == "image_region":
		var mat = ($Box.material_override as SpatialMaterial)
		var size = Vec.vec2_vec1(Vec.pairwise_div2(value.size, mat.albedo_texture.get_size()), 0)
		var pos = Vec.vec2_vec1(Vec.pairwise_div2(value.position, mat.albedo_texture.get_size()), 0)
		mat.uv1_scale = Vec.pairwise_mul3(size, Vector3(3, 2, 1))
		mat.uv1_offset =  pos - size

func set_color_locally(value):
	($Box.material_override as SpatialMaterial).albedo_color = value

func get_aabb():
	return $Box.get_transformed_aabb()

###########################
func _set_size(value):
	$NetworkEntity.set_data("size", value)
func _get_size():
	return $NetworkEntity.get_data("size", Vector3(2,2,2))

func _set_color(value):
	$NetworkEntity.set_data("color", value)
func _get_color():
	return $NetworkEntity.get_data("color", Color.white)

func _set_anchored(value):
	$NetworkEntity.set_data("anchored", value)
func _get_anchored():
	return $NetworkEntity.get_data("anchored", false)
	
func _set_image(value):
	$NetworkEntity.set_data("image", value)
func _get_image():
	return $NetworkEntity.get_data("image", "")

func _set_image_region(value):
	$NetworkEntity.set_data("image_region", value)
func _get_image_region():
	return $NetworkEntity.get_data("image_region", Rect2(0,0,0,0))
