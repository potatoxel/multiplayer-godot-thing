extends RigidBody

func _ready():
	$NetworkEntity.set_data("color", "#ff0000", false)
	$CSGSphere.material_override = $CSGSphere.material_override.duplicate(true)

func _on_NetworkEntity_on_set(key, value):
	if key == "color":
		$CSGSphere.material_override.albedo_color = value


func _on_RigidBody_mouse_entered():
	$NetworkEntity.set_data("color", "#" + Color(randf(), randf(), randf()).to_html(false))

func interact():
	$NetworkTransform.add_force(Vector3.UP*512, Vector3.ZERO)
