extends CanvasLayer

var _game_name_to_id = {}

func _ready():
	var id = 0
	Networking.connect("game_changed", self, "_on_game_changed")
	for game in FileUtils.list_files_in_directory("res://Scenes/Games/"):
		$Control/GameList.add_item(game)
		_game_name_to_id[game] = id
		id += 1
	
func _on_game_changed(game):
	$Control/GameList.select(_game_name_to_id[game])


func _on_GameList_item_activated(index):
	Networking.current_game = $Control/GameList.get_item_text(index)

func _unhandled_key_input(event):
	if event is InputEventKey:
		if event.is_action_released("ui_home"):
			$Control.visible = not $Control.visible
